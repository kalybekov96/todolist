import { ITodo } from "../interfaces";

export enum TodoActionTypes {
  CREATE_TODO = "CREATE_TODOS",
  DELETE_TODO = "DELETE_TODOS",
  UPDATE_TODO = "UPDATE_TODO",
}

interface CreateTodoAction {
  type: TodoActionTypes.CREATE_TODO;
  payload: ITodo;
}
interface DeleteTodoAction {
  type: TodoActionTypes.DELETE_TODO;
  payload: number;
}
interface UpdateTodoAction {
  type: TodoActionTypes.UPDATE_TODO;
  payload: number;
}

export type TodoAction = CreateTodoAction | DeleteTodoAction | UpdateTodoAction;

export const createTodo = (todo: ITodo): CreateTodoAction => {
  return {
    type: TodoActionTypes.CREATE_TODO,
    payload: todo,
  };
};

export const deleteTodo = (id: number): DeleteTodoAction => {
  return {
    type: TodoActionTypes.DELETE_TODO,
    payload: id,
  };
};

export const updateTodo = (id: number): UpdateTodoAction => {
  return {
    type: TodoActionTypes.UPDATE_TODO,
    payload: id,
  };
};
