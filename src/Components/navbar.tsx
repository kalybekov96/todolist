import React from "react";
import styled from "styled-components";
import { NavLink } from "react-router-dom";

const FlexDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 5px 10px;
  background-color: #5e199e;
`;

const Ul = styled.ul`
  display: flex;
  list-style: none;
  & > li {
    padding: 0 10px;
  }
`;

const LinkElem = styled(NavLink)`
  color: white;
  text-decoration: none;
`;

export const Navbar: React.FC = () => {
  return (
    <nav>
      <FlexDiv>
        <LinkElem to="/">TodoList</LinkElem>
        <Ul>
          <li>
            <LinkElem to="/">Tasks</LinkElem>
          </li>
          <li>
            <LinkElem to="/about">Information</LinkElem>
          </li>
        </Ul>
      </FlexDiv>
    </nav>
  );
};
