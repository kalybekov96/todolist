import React, { useState } from "react";
import styled from "styled-components";
import { ITodo } from "../../interfaces";

const Div = styled.div`
  padding: 1rem;
`;
const Label = styled.label`
  font-size: 14px;
  color: gray;
  margin: 10px;
  font-weight: 500;
`;
const Input = styled.input`
  font-size: 16px;
  padding: 10px;
  width: 100%;
  display: block;
  border: none;
  border-bottom: 1px solid #757575;
  &:focus {
    outline: none;
  }
`;

interface ITodoFormProps {
  onAdd(title: string): void;
  onRemove(id: number): void;
  todoList: ITodo[];
}

export const TodoForm: React.FC<ITodoFormProps> = ({
  onAdd,
  todoList,
  onRemove,
}: ITodoFormProps) => {
  const [title, setTitle] = useState<string>("");
  const changeTitle = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
  };
  const onEnterPress = (event: React.KeyboardEvent) => {
    if (event.key === "Enter") {
      let checkIndex: number = todoList.findIndex(
        (todo) => todo?.title === title
      );
      if (checkIndex !== -1) {
        const confirmHandler: boolean = window.confirm(
          "Вы точно хотиет удалить задачу?"
        );
        if (confirmHandler) {
          onRemove(todoList[checkIndex].id);
          setTitle("");
        }
      } else {
        onAdd(title);
        setTitle("");
      }
    }
  };
  return (
    <Div>
      <Label htmlFor="title">Enter task name</Label>
      <Input
        id="title"
        type="text"
        placeholder="Enter task name"
        value={title}
        onChange={changeTitle}
        onKeyPress={onEnterPress}
      />
    </Div>
  );
};
