﻿import React from "react";
import styled from "styled-components";
import { TodoForm } from "./TodoForm";
import { TodoList } from "./List";
import { ITodo } from "../../interfaces";
import { useTypedSelector } from "../hooks/useTypedSelector";
import { useDispatch } from "react-redux";
import { createTodo, deleteTodo, updateTodo } from "../../redux/actions";

const Div = styled.div`
  margin: 0 15%;
`;

export const Todo: React.FC = () => {
  const state = useTypedSelector((state) => {
    return state.todos;
  });
  const dispatch = useDispatch();

  const addTodos = (title: string) => {
    if (title.trim()) {
      const newTodo: ITodo = {
        title,
        id: Date.now(),
        completed: false,
      };
      dispatch(createTodo(newTodo));
    }
  };
  const onToggleHandler = (id: number) => {
    dispatch(updateTodo(id));
  };
  const onRemoveHandler = (id: number) => {
    dispatch(deleteTodo(id));
  };
  return (
    <Div>
      <TodoForm onAdd={addTodos} onRemove={onRemoveHandler} todoList={state} />
      <TodoList
        todos={state}
        onToggle={onToggleHandler}
        onRemove={onRemoveHandler}
      />
    </Div>
  );
};
