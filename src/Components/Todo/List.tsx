import React from "react";
import { ITodo } from "../../interfaces";
import styled from "styled-components";

const Ul = styled.ul`
  list-style: none;
  padding: 0;
  & > li > label {
    display: flex;
    align-items: center;
    justify-content: space-between;
    cursor: pointer;
    padding: 1rem;
    margin: 1rem;
    background: #f1f1f1;
  }
`;
const Icon = styled.i`
  z-index: 2;
  cursor: pointer;
  & > img {
    width: 30px;
    height: 30px;
  }
`;
const CompletedSpan = styled.span`
  text-decoration: line-through;
`;

type TodoListProps = {
  todos: ITodo[];
  onToggle(id: number): void;
  onRemove(id: number): void;
};

export const TodoList: React.FC<TodoListProps> = ({
  todos,
  onRemove,
  onToggle,
}: TodoListProps) => {
  const removeHandler = (event: React.MouseEvent, id: number) => {
    const confirmHandler: boolean = window.confirm(
      "Вы точно хотиет удалить задачу?"
    );
    if (confirmHandler) {
      event.preventDefault();
      onRemove(id);
    }
  };
  if (todos.length === 0) {
    return <p>You have no tasks yet</p>;
  }
  return (
    <Ul>
      {todos.map((todo: ITodo) => {
        return (
          <li key={todo.id}>
            <label>
              <input
                type="checkbox"
                checked={todo.completed}
                onChange={() => onToggle(todo.id)}
              />
              {todo.completed ? (
                <CompletedSpan>{todo.title}</CompletedSpan>
              ) : (
                <span>{todo.title}</span>
              )}
              <Icon onClick={(e) => removeHandler(e, todo.id)}>
                <img
                  src="https://img.icons8.com/plasticine/100/000000/filled-trash.png"
                  alt="delete-icon"
                />
              </Icon>
            </label>
          </li>
        );
      })}
    </Ul>
  );
};
