import { TodoAction, TodoActionTypes } from "./actions";
import { ITodo } from "../interfaces";

const initialState: ITodo[] = [];

export const todosReducer = (state = initialState, action: TodoAction) => {
  switch (action.type) {
    case TodoActionTypes.CREATE_TODO:
      return [...state, action.payload];
    case TodoActionTypes.DELETE_TODO:
      return state.filter((item) => action.payload !== item.id);
    case TodoActionTypes.UPDATE_TODO:
      return state.map((todo) => {
        if (todo.id === action.payload) {
          return { ...todo, completed: !todo.completed };
        }
        return todo;
      });
    default:
      return state;
  }
};
